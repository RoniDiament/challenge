# README #

### Live preview ###

Link: 
[https://challenge-8astpbytc-ronidiam.vercel.app/](https://challenge-8astpbytc-ronidiam.vercel.app/)

### Proceso e inconvenientes ###

* Primero arranqué poniendo un circulo con otro adentro que va girando. Ahí aprendí a hacer una rotación en animación
* Luego, realicé las formas (triangulo, trapecio, cuadrado, luna). Para el trapecio por ejemplo, hice primero un rectangulo y luego "estiré" las puntas
* Luego, las rotaciones del triangulo y del cuadrado las quería hacer que roten tocando el circulo contenedor pero me quedaron afuera. Entonces tuve que ponerlos adentro (display:inline-block;)
* Para terminar, quise jugar con los tiempos de rotación. Entonces fui cambiandolos para ver como se comportaban

### Research de las propiedades de CSS usadas ###

* Para la animación creé un keyframe girar donde indico desde y hacia donde es el giro. Luego, en los elementos que quiero, le aplico esta animación indicando la cantidad de segundos de esa animación, que en este caso es el giro. Le indico también que quiero que se repita todo el tiempo y que sea linear, es decir, que se mueva constante e igual
* Para hacer el trapecio, como explicaba arriba, hice primero un rectangulo y luego agregué los borders left y right para darle la forma 